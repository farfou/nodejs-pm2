# Nodejs Pm2

nodejs-pm2

## Getting started

## Sample Kubectl Commands


## Sample Helm Commands


helm install --create-namespace --namespace <namespace> <release-name> <chart_path>
ex:
helm install --create-namespace --namespace pm2-helm-1 release-1 .


helm upgrade --namespace <namespace> <release-name> <chart_path>
ex:
helm upgrade --namespace pm2-helm-1 release-1 .


helm upgrade --namespace <namespace> <release-name> <chart_path>
ex:
helm upgrade --namespace pm2-helm-1 release-1 .



## Deploy Prometheus / Grafana on Kube
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install prometheus-community/kube-prometheus-stack \
--create-namespace --namespace prometheus \
--generate-name \
--set prometheus.prometheusSpec.serviceMonitorSelectorNilUsesHelmValues=false


## PM2 Metrics For Prometheus
https://github.com/saikatharryc/pm2-prometheus-exporter
To expose /metrics on PM2 App for Prometheus Scraping


## Check file: kubernetes/prometheus-grafana/pm2-service-monitor.yaml
To create a Prometheus Scrapping rule for PM2







