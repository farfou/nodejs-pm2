const http = require('http');
var path = require('path');
var scriptName = path.basename(__filename);


function log(message){
    console.log('[' + scriptName + '] ' + message);
}



process.on('SIGINT', function() {

     log('closing instance' + process.env.NODE_APP_INSTANCE);
     process.exit(0);
});

let app = http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello from ' + process.env.NODE_APP_INSTANCE);
});

let port = process.env.PORT;
app.listen(process.env.PORT, '127.0.0.1');
log('Node server running on port ' + port);
