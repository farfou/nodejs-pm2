const http = require('http');
const tx2 = require('tx2');
var path = require('path');
var scriptName = path.basename(__filename);


function log(message){
    console.log('[' + scriptName + ': ' + process.env.APPLICATION_INSTANCE_ID + '] ' + message);
}

const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));


log('starting app with ' + process.env.ENV_NAME);

let meter = tx2.meter({
    name: 'my_metric',
    samples: 1,
    timeframe: 60
})

var liveReqCount = tx2.counter({
  name : 'live requests'
})


process.on('SIGINT', function() {
     log('received int signal');
     sleep(5000).then(() => {
        log('app closed finish');
        process.exit(0);
     })
});


let app = http.createServer((req, res) => {
    liveReqCount.inc()
    meter.mark();
    req.on('end', function(){
        liveReqCount.dec()
    })
    log('processing request from ' + req.connection.remoteAddress);
    sleep(500).then(() => {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('request processing '+ process.env.APPLICATION_INSTANCE_ID + '\n');
    })
    
    
});

let port = process.env.PORT;


log('starting app init');
// ICI CODE D'intialisation
sleep(5000).then(() => {
    log('app init done')
    var listener = app.listen(process.env.PORT, '127.0.0.1', function(){
        log('app is running on ' + listener.address().port);
        process.send('ready');
        log('app is now ready');
    });
});