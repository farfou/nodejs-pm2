module.exports = {
  apps : [{
    name   : "app-cluster-1",
    script : "./app-with-config.js",
    exec_mode: "cluster",
    "instances": 4,
    "instance_var": "APPLICATION_INSTANCE_ID",
    wait_ready: true,
    listen_timeout: 6000,
    kill_timeout: 3000,
    "env": {
      "PORT": 6001,
      "ENV_NAME": "default"
    },
    "env_toto": {
      "ENV_NAME": "toto"
    },
    "env_tutu": {
      "ENV_NAME": "tutu"
    }
  }]
}
