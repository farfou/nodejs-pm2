const http = require('http');
const tx2 = require('tx2');
var path = require('path');
var scriptName = path.basename(__filename);


function log(message){
    console.log('[' + scriptName + ': ' + process.env.APPLICATION_INSTANCE_ID + '] ' + message);
}

const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));


log('starting app with ' + process.env.ENV_NAME);

let meter = tx2.meter({
    name: 'my_metric',
    samples: 1,
    timeframe: 60
})

var liveReqCount = tx2.counter({
  name : 'live requests'
})


var totalReqCount = tx2.counter({
  name : 'total requests'
})

process.on('SIGINT', function() {
     log('received int signal');
     sleep(5000).then(() => {
        log('app closed finish');
        process.exit(0);
     })
});


let podName = process.env.MY_POD_NAME;
let nodeName = process.env.MY_NODE_NAME;
let podNamespace = process.env.MY_POD_NAMESPACE;



let app = http.createServer((req, res) => {
    liveReqCount.inc()
    totalReqCount.inc()
    meter.mark();
    req.on('end', function(){
        liveReqCount.dec()
    })
    log('processing request from ' + req.connection.remoteAddress);
    sleep(500).then(() => {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end('<html><body>Test.<br />response sent from:<br /> PM2 Instance: '
            + process.env.APPLICATION_INSTANCE_ID + '<br /> Pod Namespace: '
            + podNamespace + '<br />Node: ' + nodeName
            + '</body></html>');
    })
    
    
});

let port = process.env.PORT;


log('starting app init');
// ICI CODE D'intialisation
sleep(5000).then(() => {
    log('app init done')
    var listener = app.listen(process.env.PORT, function(){
        log('app is running on ' + listener.address().port);
        process.send('ready');
        log('app is now ready');
    });
});