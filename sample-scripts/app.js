const http = require('http');
var path = require('path');
var scriptName = path.basename(__filename);
function log(message){
    console.log('[' + scriptName + '] ' + message);
}

process.on('SIGINT', function() {

     log('closing instance' + process.env.NODE_APP_INSTANCE);
     process.exit(0);
});

let app = http.createServer((req, res) => {
    // Set a response type of plain text for the response
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello from ' + process.env.NODE_APP_INSTANCE);
});

app.listen(3000, '127.0.0.1');
log('Node server running on port 3000');
